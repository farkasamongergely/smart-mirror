from __future__ import division
class Sorozatok(object):
    @staticmethod
    def szamtani_sorozat_n_eleme(a1, n, d):
        an = a1 + (n - 1) * d
        return an

    @staticmethod
    def szamtani_sorozat_elso_n_osszege(a1, an, n):
        sn = ((a1 + an) * n) / 2
        return sn

    @staticmethod
    def mertani_sorozat_n_eleme(a1, q, n):
        if q != 1:
            return a1 * pow(q, n - 1)
        else:
            return a1

    @staticmethod
    def mertani_sorozat_elso_n_osszege(a1, q, n):
        if q != 1:
            return a1 * ((pow(q, n) - 1) / q - 1)
        else:
            return n * a1


if __name__ == "__main__":
    # print Sorozatok.szamtani_sorozat_n_eleme(100, 3, -5)
    # print Sorozatok.szamtani_sorozat_elso_n_osszege(1,40,40)
    #print Sorozatok.szamtani_sorozat_elso_n_osszege(13, Sorozatok.szamtani_sorozat_n_eleme(13,150,4), 150)
    print Sorozatok.szamtani_sorozat_elso_n_osszege(200000,435000,48)